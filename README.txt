********************************************************************
                        TIME CLASS MODULE
********************************************************************

INTRODUCTION
--------------------------------------------------------------------
The Time Class module adds a simple configuration page on which you
can add settings for output time classes to your <body> tag.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/time_class
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/time_class

REQUIREMENTS
--------------------------------------------------------------------
 * This module does not require installation of additional modules.

RECOMMENDED MODULES
--------------------------------------------------------------------
 * There are no recommended modules.

INSTALLATION
--------------------------------------------------------------------
 * Install as you would normally install a contributed Drupal module.
   See:
   https://drupal.org/documentation/install/modules-themes/modules-5-6
   for further information.

CONFIGURATION
--------------------------------------------------------------------
 * Go to /admin/config/development/time_class to configure the module
   after installation.

MAINTAINERS
--------------------------------------------------------------------
 Current maintainer:
 * Andrey Tsirkun - https://www.drupal.org/u/torvald
